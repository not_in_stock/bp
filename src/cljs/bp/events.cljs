(ns bp.events
  (:require [re-frame.core :as re-frame]
            [bp.db :as db]))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::change-name
 (fn [db [_event-name value]]
   (assoc db :name value)))

(re-frame/reg-event-fx
 ::spawn-notification
 (fn [_ [_event-name value]]
   (js/alert value)))
