(ns bp.subs
  (:require [re-frame.core :as re-frame]
            [clojure.string :as str]))

(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))

(re-frame/reg-sub
 ::result
 (fn [db]
   (:result db)))

(def path-keys
  [:db/id :name :type])

(defn branch? [node]
  (or (contains? node :children)
      (= :folder (:type node))))

(defn order-nodes [nodes]
  (reverse (tree-seq branch? :children nodes)))

(defn leaf? [node]
  (= :course (:type node)))

(defn collect-paths [paths node]
  (cond
    (leaf? node) (conj paths (vector (select-keys node path-keys)))
    (branch? node) (for [path paths]
                     (conj path (select-keys node path-keys)))))

(re-frame/reg-sub
 ::course-paths
 :<- [::result]
 (fn [nodes]
   (reduce collect-paths
           []
           (order-nodes nodes))))

(re-frame/reg-sub
 ::path+course
 :<- [::course-paths]
 (fn [paths]
   (for [path paths
         :let [[course & folders] path
               string-path (->> folders
                                reverse
                                (map #(or (:name %) "~"))
                                (str/join "/"))]]
     [string-path (:name course)])))
