(ns bp.views
  (:require [re-frame.core :as re-frame]
            [bp.subs :as subs]))

(defn course-table [course-tuples]
  [:table
   [:thead
    [:tr
     [:th "Course"]
     [:th "Path"]]]
   (into [:tbody]
         (for [[course-name course-path] course-tuples]
           [:tr
            [:td course-name]
            [:td course-path]]))])

(defn main-panel []
  (let [*path+course (re-frame/subscribe [::subs/path+course])]
    [:div
     [course-table @*path+course]]))
